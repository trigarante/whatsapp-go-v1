import {Request, Response, Router} from "express";
import crearCanalWhatsapp from "../../queries/plantillasQueries/crearCanalWhatsapp";
import TipoSubareaPlantillaWhatsapp from "../../queries/tipoSubareaPlantillaWhatsapp/tipoSubareaPlantillaWhatsapp";
import Canales from "../../queries/canales/canales";
import CatalogoNumerosSinch from "../../queries/numerosSinch/CatalogoNumerosSinch";
import https from 'https';
import CrearCanalWhatsapp from "../../queries/plantillasQueries/crearCanalWhatsapp";
import Empleados from "../../queries/empleados/empleados";

const PlantillasRoutes = Router();

PlantillasRoutes.get('/getCanal', async (req: Request, res: Response) => {
    const test = await crearCanalWhatsapp.getCanalWhatsapp(1);
    res.status(200).send(test);
});

PlantillasRoutes.post('/send-hsm', async (req: Request, res: Response) => {
    const idEmpleado = req.body.idEmpleado;
    // const idTipoSubarea = req.body.idTipoSubarea; // queda pendiente ver como se va a manjear el idTipoSubarea debtro de GO
    const idSolicitud = req.body.messages.destinations[0].correlationId;
    let cuerpoPlantilla: string;
    let numeroDestino = req.body.messages.destinations[0].destination;
    numeroDestino = numeroDestino.slice(3);

    const {messages} = req.body;
    const {message} = messages;
    const {template} = message;

    const { idTipoSubarea } = await TipoSubareaPlantillaWhatsapp.getEmpleadoT2020(idEmpleado);
    if (idTipoSubarea === undefined) {
        res.status(400).send({
            mensaje: 'Verificar el idEmpleado que se está enviando',
            error: 'Empleado no encontrado',
            empleado: idEmpleado,
            encontrado: false
        });
        return;
    }

    const tipoSubareaData: any = await TipoSubareaPlantillaWhatsapp.getPlantillaByIdTipoSubareaActivoAndIdTipoPlantilla(idTipoSubarea);

    if (!tipoSubareaData) {
        return res.status(404).json({
            error: 'El tipo subarea del empleado no está registrado para poder mandar plantillas',
            mensaje: 'Agregar tipo subarea a la tabla tipoSubareaPlantillaWhatsapp para poder enviar plantilla',
            extra: `tipo subarea intentando enviar plantilla: ${idTipoSubarea}`,
        });
    }

    template.elementName = tipoSubareaData.nombrePlantilla;
    cuerpoPlantilla = tipoSubareaData.cuerpo;

    const {
        nombre, apellidoPaterno, apellidoMaterno, nombreCompleto
    } = await TipoSubareaPlantillaWhatsapp.getNombreEmpleadoNombreProspecto(idSolicitud);

    template.bodyParameters[0] = `${nombreCompleto}`;
    template.bodyParameters[1] = `${nombre} ${apellidoPaterno} ${apellidoMaterno}`;

    template.languagePolicy = 'DETERMINISTIC';
    template.namespace = '0afd3994_d014_45ab_830e_1613b060a7ec';

    const templateConHeader = {...template};
    req.body.messages.message.template = {...templateConHeader};

    const dataCanal: any = await Canales.getCanalByDestino(numeroDestino);
    const dataCatalogoNumerosWavy: any = await CatalogoNumerosSinch.getCatalogos();
    let credentials;

    if (dataCanal.found === 0) {
        credentials = await CatalogoNumerosSinch.numeroDataRandom(dataCatalogoNumerosWavy, null, numeroDestino);
        const jsonCredential = {
            credentials: {...credentials}
        };
        req.body = {...req.body, ...jsonCredential, cuerpoPlantilla};
    } else {
        let numerosRestantes;
        const last = dataCanal.data.length - 1;
        const numeroOrigen = dataCanal.data[last].origen;
        const destinoNumero = dataCanal.data[last].destino;

        const dataOrigenDestino = await Canales.getOrigenDestino(destinoNumero, numeroOrigen);
        numerosRestantes = dataOrigenDestino.numerosRestantes;

        if (numerosRestantes.length !== 0) {
            const numerosRestantesData = await CatalogoNumerosSinch.numeroDataRandom(numerosRestantes, null, destinoNumero);

            req.body = {...req.body, cuerpoPlantilla, credentials: {numerosRestantes: {}, numeroGenerador: numerosRestantesData.numeroGenerador, userName: numerosRestantesData.userName, namespace: numerosRestantesData.namespace, authToken: numerosRestantesData.authToken}};
            req.body.credentials.numerosRestantes = numerosRestantes;
        } else {
            res.status(200).send({
                status: `El número ${destinoNumero} ya alcanzó el límite permitido al usar ya los 5 números disponibles`,
                message: 'Sin numeros disponibles',
                sinNumerosTelefonicos: true
            });
            return;
        }
    }

    // Unicamente para test, se descomentan estas tres lineas y se comenta todo lo de abajo
    /*await CrearCanalWhatsapp.postCanalWhatsPlantillaExterno(req, res).then((pcwpe: any) => {
        res.status(200).send({canalWhatsapp: pcwpe.canalWhatsapp, mensajesWhatsapp: pcwpe.mensajesWhatsapp});
    });*/

    const headers = {
        'Content-Type': 'application/json',
        'UserName': `${req.body.credentials.userName}`,
        'AuthenticationToken': `${req.body.credentials.authToken}`,
    };
    const data = JSON.stringify(req.body.messages);

    const options: any = {
        headers,
        method: 'POST',
        path: '/v1/whatsapp/send',
        body: JSON.stringify(data),
        host: 'api-messaging.wavy.global',
    };

    const reeq = https.request(options, rees => {
        let payload = '';

        // se reciben los pedazos de info y se añade a 'str'
        rees.on('data', d => {
            payload += d;
        });
        // toda la respuesta ha sigo recibida, se manda en formato json
        rees.on('end', async () => {
            await CrearCanalWhatsapp.postCanalWhatsPlantillaExterno(req, res).then((pcwpe: any) => {
                res.status(200).send({canalWhatsapp: pcwpe.canalWhatsapp, mensajesWhatsapp: pcwpe.mensajesWhatsapp});
            });
        });
    });
    reeq.on('error', error => {
        // manejador de errores
        const erroor = {...error, mensaje: 'Plantilla no enviada'};
        res.send(erroor);
    });
    reeq.write(data);
    reeq.end();
});
// falta checar
PlantillasRoutes.get('/plantillas-whatsapp/tipo-subarea/:idEmpleado', async (req: Request, res: Response) => {
    const idEmpleado = +req.params.idEmpleado;
    try {
        const { idTipoSubarea } = await Empleados.getEmpleadoById(idEmpleado);
        const tipoPlantilla = await crearCanalWhatsapp.getPlantillaByIdEmpleadoTipoSubarea(idTipoSubarea);
        res.status(200).send(tipoPlantilla);
    } catch (e) {
        res.status(500).send({mensaje: `tiposubarea sin plantillas para envío`});
    }
});


export default PlantillasRoutes;
