import {Request, Response, Router} from "express";
import CanalWhatsapp from "../../models/CanalWhatsapp";
import MensajesWhatsapp from "../../models/MensajesWhatsapp";
import CrearCanalWhatsapp from "../../queries/plantillasQueries/crearCanalWhatsapp";

const CanalesWhatsappRoutes = Router();

CanalesWhatsappRoutes.post('/solicitudes/numero-prospecto/:idSolicitud/:idEmpleados', async (req: Request, res: Response) => {
    const idEmpleados = req.params.idEmpleados;
    const idSolicitud = req.params.idSolicitud;
    req.params = {idEmpleados, idSolicitud};
    await CrearCanalWhatsapp.createCanalWhatsapp(req, res);
});

export default CanalesWhatsappRoutes;
