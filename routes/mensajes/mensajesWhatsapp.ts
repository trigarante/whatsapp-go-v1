import {Request, Response, Router} from "express";
import canales from "../../queries/canales/canales";
import Mensajes from "../../queries/Mensajes/Mensajes";
import CatalogoNumerosSinch from "../../queries/numerosSinch/CatalogoNumerosSinch";

// tslint:disable-next-line:no-var-requires
const https = require('https');


const MensajesWhatsapp = Router();

MensajesWhatsapp.get('/mensajes-canal-general/:idEmpleado', async (req: Request, res: Response) => {
    const idEmpleado = +req.params.idEmpleado;

    try {
        const mensajes = await Mensajes.getMensajesByIdEmpleadoBandejaTipoEnvio(idEmpleado);

        // const mensajesFiltroHora = mensajes.filter( (data: any) => {
        //     const date = new Date(data.fecha);
        //     const dateConHoras = new Date(data.fecha).setHours(24);
        //     const dateActual = new Date();
        //     const dateMsecActual = dateActual.getTime();
        //
        //     const fechasData = timeConversion1(date, dateConHoras, dateMsecActual);
        //     return fechasData.milisecActual < fechasData.milisecFechaRegistroMasHoras;
        // });
        const noRespondidosArr: any[] = [];
        mensajes.forEach((dataMsg: any) => {
            noRespondidosArr.push(dataMsg.correlationId);
        });
        const noRespondidosArrSorted = noRespondidosArr.sort();
        const counter = {};
        // tslint:disable-next-line:prefer-for-of
        for(let i = 0; i < noRespondidosArrSorted.length; i++) {
            if(!(noRespondidosArrSorted[i] in counter)){ // @ts-ignore
                counter[noRespondidosArrSorted[i]] = 0;
            }
            // @ts-ignore
            counter[noRespondidosArrSorted[i]]++;
        }
        const setNoRespondidosArr = new Set(noRespondidosArr);

        const dataNoRepetida = noRespondidosArr.filter( onlyUnique1 );

        // pendiente, para poder sacar la fecha y mostrarla en la bandeja
        const paraFechaArr = [];
        const dataClasificada = dataNoRepetida.map(d => {
            return mensajes.filter((val: any) => {
                return val.correlationId === d
            });
        });
        // tslint:disable-next-line:prefer-for-of
        for (let i = 0; i < dataClasificada.length; i++) {
            // tslint:disable-next-line:prefer-for-of
            for (let j = 0; j < dataClasificada[i].length; j++) {
                if (dataClasificada[i].length > 1 ) {
                    paraFechaArr.push(dataClasificada[dataClasificada.length - 1]);
                } else {
                    paraFechaArr.push(dataClasificada[0]);
                }
            }
        }

        if (mensajes) {
            const json = {
                mensajes: dataNoRepetida,
                cantidadNotificaciones: setNoRespondidosArr.size,
                // socketNumero: 3,
            }; // AQUIIIIIIIIIII
            req.body = {...req.body, ...json};
            res.send({mensajes: dataNoRepetida, cantidadNotificaciones: setNoRespondidosArr.size, cantidadMensajesIndividuales: counter, dataClasificada});
        }
    } catch (e) {
        console.log(e);
    }

    function onlyUnique1(value: any, index: any, self: string | any[]) {
        return self.indexOf(value) === index;
    }

    function timeConversion1(fecha: any, fechaMasHora: any, dateMsec: any) {
        const msecPerMinute = 1000 * 60;
        const msecPerHour = msecPerMinute * 60;
        const msecPerDay = msecPerHour * 24;

        const milisecFechaRegistro = fecha / msecPerDay;
        const milisecFechaRegistroMasHoras = fechaMasHora / msecPerDay;
        const milisecActual = dateMsec / msecPerDay;

        return {
            milisecFechaRegistro,
            milisecFechaRegistroMasHoras,
            milisecActual
        }
    }
});

MensajesWhatsapp.post('/send', async(req: Request, res: Response) => {
    /*** Manda unicamente mensajes **/
        const headers = {
            'Content-Type': 'application/json',
            'UserName': `${req.body.credentials.userName}`,
            'AuthenticationToken': `${req.body.credentials.authToken}`,
        };
        // otra manera de hacer la petición sin libreria
        const data = JSON.stringify(req.body.message);
        // const data = JSON.stringify(req.body);
        const options: any = {
            headers,
            method: 'POST',
            path: '/v1/whatsapp/send',
            body: JSON.stringify(data),
            host: 'api-messaging.wavy.global',
        };
        const reeq = https.request(options, (rees: any) => {
            let payload = '';

            // se reciben los pedazos de info y se añade a 'str'
            rees.on('data', (d: any) => {
                payload += d;
            });
            // toda la respuesta ha sigo recibida, se manda en formato json
            rees.on('end', () => {
                res.json(JSON.parse(payload));
            });
        });
        reeq.on('error', (error: any) => {
            // manejador de errores
            const erroor = {...error, mensaje: 'algo salió mal'};
            res.send(erroor);
        });
        reeq.write(data);
        reeq.end();
});

MensajesWhatsapp.get('/mensajes/canal', async(req: Request, res: Response) => {
    try {
        const idSolicitud: number | any = req.query.idSolicitud;
        const canal = await canales.getCanalByIdSolicitud(idSolicitud);
        const mensajes = await Mensajes.getMensajesByIdCanal(canal.id);
        const dataCatalogo = await CatalogoNumerosSinch.getCatalogoByTelefono(+canal.origen);
        const solicitudData = await Mensajes.getSolicitudById(idSolicitud);
        const canalConNumeroWavy = await canales.getCanalByNumeroProspecto(solicitudData.telefono);

        const cantidadPlantillas = canalConNumeroWavy.length;

        if (!canal) {
            return res.json({
                cuerpo: null,
                mensaje: `No se encontró ningún canal con el idSolicitud: ${idSolicitud}`,
                cantidadDeEnvioDePlantillas: cantidadPlantillas
            });
        }
        if (!mensajes) {
            return res.json({
                cuerpo: null,
                mensaje: 'Todavia no hay mensajes en el canal',
            });
        }
        if (canal && mensajes) {
            res.send({mensajes, catalogoData: dataCatalogo, canalData: canal, solicitudData});
        }
    } catch (e) {
        console.log(e);
    }
});

MensajesWhatsapp.put('/mensajes-respondido/:idEmpleado', async(req: Request, res: Response) => {
    const mensajesData = req.body;
    const mensajesActualizados: any[] = [];

    for (let i = 0; i < mensajesData.length; i++) {
        try {
            if (mensajesData[i].aBandeja === 1) {
                mensajesData[i] = {...mensajesData[i], aBandeja: 0};
                const mensajes = await Mensajes.updateMensajesRespondidosVistos(mensajesData[i].id, mensajesData[i]);
                mensajesActualizados.push(mensajes);
            }
        } catch (e) {
            console.log(e);
        }
    }

    await getMensajesAndCanal().then( (val: any) => {
        res.status(200).json({mensajesActualizados});
    }).catch((e: any) => {
        console.log(e, 'eeeeeeeeeerrrrrrrrrrrrrrrroooooooooooor 1111111111111111');
    });

    async function getMensajesAndCanal() {

        try {
            const mensajes = await Mensajes.getMensajesBandejaTipoEnvio();
            // const mensajesFiltroHora = mensajes.filter((data: any) => {
            //     const date = new Date(data.fecha);
            //     const dateConHoras = new Date(data.fecha).setHours(24);
            //     const dateActual = new Date();
            //     const dateMsecActual = dateActual.getTime();
            //
            //     const fechasData = timeConversion(date, dateConHoras, dateMsecActual);
            //     return fechasData.milisecActual < fechasData.milisecFechaRegistroMasHoras;
            // });
            const noRespondidosArr: any[] = [];
            mensajes.forEach((dataMsg: any) => {
                noRespondidosArr.push(dataMsg.correlationId);
            });
            const setNoRespondidosArr = new Set(noRespondidosArr);
            const dataNoRepetida = noRespondidosArr.filter(onlyUnique);
            if (mensajes) {
                const json = {
                    mensajes: dataNoRepetida,
                    cantidadNotificaciones: setNoRespondidosArr.size,
                    // socketNumero: 3,
                };
                req.body = {...req.body, ...json};
            }
        } catch (e) {
            console.log(e);
        }

        function onlyUnique(value: any, index: any, self: string | any[]) {
            return self.indexOf(value) === index;
        }

        function timeConversion(fecha: any, fechaMasHora: any, dateMsec: any) {
            const msecPerMinute = 1000 * 60;
            const msecPerHour = msecPerMinute * 60;
            const msecPerDay = msecPerHour * 24;

            const milisecFechaRegistro = fecha / msecPerDay;
            const milisecFechaRegistroMasHoras = fechaMasHora / msecPerDay;
            const milisecActual = dateMsec / msecPerDay;

            return {
                milisecFechaRegistro,
                milisecFechaRegistroMasHoras,
                milisecActual
            }
        }
    }
});

export default MensajesWhatsapp;
