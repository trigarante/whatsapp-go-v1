import {Request, Response, Router} from "express";
import MensajesWhatsapp from "../mensajes/mensajesWhatsapp";
import Empleados from "../../queries/empleados/empleados";

const MonitoreoGo = Router();

MonitoreoGo.get('/ejecutivos-go', async (req: Request, res: Response) => {
    const fechaInicio = req.query.fechaInicio;
    const fechaFin = req.query.fechaFin;

    const ejecutivosCanales = await Empleados.getEjecutivosGo();

    res.status(200).send( ejecutivosCanales );
});

MonitoreoGo.get('/ejecutivos-canales-go/:idEmpleado', async (req: Request, res: Response) => {
    const fechaInicio = req.query.fechaInicio;
    const fechaFin = req.query.fechaFin;
    const idEmpleado = +req.params.idEmpleado;

    const ejecutivosCanales = await Empleados.getEmpleadosMensajesWhatsappGo(idEmpleado, fechaInicio, fechaFin);

    res.status(200).send( ejecutivosCanales );
});

export default MonitoreoGo;
