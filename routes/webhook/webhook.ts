import {Request, Response, Router} from "express";
import CallbackSinch from "../../models/CallbackSinch";
import SolicitudesWebhook from "../../queries/webhook/SolicitudesWebhook";
import CrearCanalWhatsapp from "../../queries/plantillasQueries/crearCanalWhatsapp";

const Callbackroutes = Router();

Callbackroutes.post('/save', async (req: Request, res: Response, next) => {
    const { data } = req.body;
    const idSinch = req.body.data[0].id;
    const idSolicitud = req.body.data[0].correlationId;

    const callbackSinch: any = new CallbackSinch();
    callbackSinch.data = data;
    callbackSinch.idSinch = idSinch;
    callbackSinch.idSolicitud = idSolicitud;

    const callbackData: any = await CallbackSinch.create({...callbackSinch.dataValues});
    res.status(200).send({status: 'callback registrado'});

    const { idEmpleados } = await SolicitudesWebhook.getSolicitudesCompletas(idSolicitud).then(nextRes => {
        return !nextRes ? {idEmpleados: null} : {...nextRes};
    });
    if (!idEmpleados) { return console.log({mensaje: 'Una solicitud no debe tener el idEmpleado nulo', error: `Solicitud ${idSolicitud} con empleado ${idEmpleados}`}); }
    req.params = {idEmpleados};
    try {
        req.body = {...req.body, 'idTipoEnvio': 2};
        // tslint:disable-next-line:only-arrow-functions no-unused-expression
        await new Promise(async function (resolve, reject) {
            return await CrearCanalWhatsapp.createCanalWhatsapp(req, res)
                .then((resCanal: any) => {
                    resolve(resCanal);
                })
                .catch(e => {
                    reject(({mensaje: `Callback ${callbackData.dataValues.id} no registrado`, error: e}));
                });
        });
    } catch (e) {
        next(e);
    }
});

export default Callbackroutes;
