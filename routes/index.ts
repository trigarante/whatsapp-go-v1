import {Request, Response, Router} from 'express';
import Webhook from './webhook/webhook';
import Plantillas from './plantillas/plantillas';
import MensajesWhatsapp from "./mensajes/mensajesWhatsapp";
import CanalesWhatsappRoutes from "./canales/canalesWhatsapp";
import MonitoreoGo from "./monitoreo/monitoreo-go";

const routes = Router();

routes.use("/callback", Webhook);
routes.use("/plantillas", Plantillas);
routes.use("/canales", CanalesWhatsappRoutes);
routes.use("/mesajes", MensajesWhatsapp);
routes.use("/monitoreo", MonitoreoGo);

export default routes;
