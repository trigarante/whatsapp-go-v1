import db from "../../db/connection";
import {QueryTypes} from "sequelize";

export default class CatalogoNumerosSinch {
    static async getCatalogos(): Promise<any> {
        return await db.dbGoAgentes.query(`
            SELECT
                catalogoNumerosSinch.*
            FROM
                catalogoNumerosSinch
            WHERE
                catalogoNumerosSinch.idMarca = 1
        `, {type: QueryTypes.SELECT});
    }

    static async getCatalogoByTelefono(numeroTelefono: number): Promise<any> {
        return await db.dbGoAgentes.query(`
            SELECT
                *
            FROM
                catalogoNumerosSinch
            WHERE
                catalogoNumerosSinch.numeroTelefono = ${numeroTelefono}
        `, {
            type: QueryTypes.SELECT,
            plain: true
        });
    }

    static async numeroDataRandom(arr: any, numeroDestino?: any, numCliente?: any) {
        return await new Promise<any>(async (resolve, reject) => {
            if (numeroDestino) {
                let index;
                let size = 0;
                for (let i = 0; i < arr.length; i++) {
                    size = i;
                }
                index = Math.floor(Math.random() * (size));
                const daraReturn = arr.splice(index, 1);
                const arrNumerosWavyDb = {
                    numeroGenerador: daraReturn[0].numeroTelefono,
                    numerosRestantes: arr,
                    namespace: daraReturn[0].namespace,
                    userName: daraReturn[0].userName,
                    authToken: daraReturn[0].authToken,
                    id: daraReturn[0].id,
                };
                resolve([] ? null : arrNumerosWavyDb);
            } else {
                let index;
                let size = 0;
                for (let i = 0; i < arr.length; i++) {
                    size = i;
                }
                index = Math.floor(Math.random() * (size));

                const daraReturn = arr.splice(index, 1);
                const arrNumerosWavyDb = {
                    numeroGenerador: daraReturn[0].numeroTelefono,
                    numerosRestantes: arr,
                    namespace: daraReturn[0].namespace,
                    userName: daraReturn[0].userName,
                    authToken: daraReturn[0].authToken,
                    id: daraReturn[0].id,
                };
                resolve(arrNumerosWavyDb);
            }
        });
    }
}
