import db from "../../db/connection";
import {QueryTypes} from "sequelize";


export default class Canales {
    static async getCanalByIdSolicitud(idSolicitud: number): Promise<any> {
        return await db.dbGoAgentes.query(`
            SELECT
                canalWhatsapp.*
            FROM
                canalWhatsapp
            WHERE
                canalWhatsapp.idSolicitud = ${idSolicitud}
        `, {
            type: QueryTypes.SELECT,
            plain: true
        });
    }

    static async getCanalByDestino(numDestino: number) {
        return await new Promise<any>(async (resolve, reject) => {
            try {
                const canalEncontrado = await db.dbGoAgentes.query(`
            SELECT
                canalWhatsapp.*
            FROM
                canalWhatsapp
            WHERE
                canalWhatsapp.destino = ${numDestino}
        `, {type: QueryTypes.SELECT});

                if (canalEncontrado.length !== 0) {
                    resolve({
                        found: 1,
                        data: canalEncontrado,
                    });
                } else {
                    resolve({found: 0});
                }
            } catch (e) {
                reject({
                    message: `El número ${numDestino} no contiene ninguna información dentro de la tabla canalWhatsapp`,
                    error: e,
                });
            }
        });
    }

    static async getOrigenDestino(numDestino: number, numOrigen: number) {
        return await new Promise<any>(async (resolve, reject) => {
            try {
                const canalEncontrado: any = await db.dbGoAgentes.query(`
            SELECT
                canalWhatsapp.*
            FROM
                canalWhatsapp
            WHERE
                canalWhatsapp.destino = ${numDestino} AND
                canalWhatsapp.origen = ${numOrigen}
        `, {
                    type: QueryTypes.SELECT,
                    plain: true
                });

                if (canalEncontrado) {
                    resolve({
                        // data: canalEncontrado
                        found: 1,
                        solicitud: canalEncontrado.idSolicitud,
                        destino: canalEncontrado.destino,
                        idCanal: canalEncontrado.id,
                        numeroOrigen: canalEncontrado.origen,
                        numerosRestantes: canalEncontrado.numerosRestantes
                    });
                } else {
                    resolve({found: 0});
                }
            } catch (e) {
                reject({
                    message: `No se encontró información con el número destino: ${numDestino}
                     y número origen ${numOrigen}`,
                    error: e,
                });
            }
        });
    }

    static async getCanalByNumeroProspecto(numeroProspecto: number): Promise<any> {
        return await db.dbGoAgentes.query(`
            SELECT
                canalWhatsapp.*
            FROM
                canalWhatsapp
            WHERE
                canalWhatsapp.destino = ${numeroProspecto}
        `, {
            type: QueryTypes.SELECT
        });
    }
}
