import db from "../../db/connection";
import {Op, QueryTypes} from "sequelize";

export default class TipoSubareaPlantillaWhatsapp {
    static async getPlantillaByIdTipoSubareaActivoAndIdTipoPlantilla (idTipoSubarea: number): Promise<any> {
        return await db.dbGoAgentes.query(`
            SELECT
                plantillaComercialWhatsappGo.id,
                plantillaComercialWhatsappGo.nombrePlantilla,
                plantillaComercialWhatsappGo.cuerpo,
                plantillaComercialWhatsappGo.activa,
                plantillaComercialWhatsappGo.fechaRegistro,
                plantillaComercialWhatsappGo.categoria,
                plantillaComercialWhatsappGo.lenguaje,
                plantillaComercialWhatsappGo.tipoEncabezado,
                plantillaComercialWhatsappGo.encabezado,
                plantillaComercialWhatsappGo.piePagina,
                plantillaComercialWhatsappGo.boton,
                plantillaComercialWhatsappGo.botonesData,
                tipoSubareaPlantillaWhatsappGo.idTipoSubarea,
                tipoSubareaPlantillaWhatsappGo.activo,
                tipoSubareaPlantillaWhatsappGo.idTipoPlantilla
                FROM
                plantillaComercialWhatsappGo
                JOIN
                    tipoSubareaPlantillaWhatsappGo ON plantillaComercialWhatsappGo.id = tipoSubareaPlantillaWhatsappGo.idPlantillaComercialWhatsapp
                WHERE
                    tipoSubareaPlantillaWhatsappGo.idTipoSubarea = ${idTipoSubarea} AND
                    tipoSubareaPlantillaWhatsappGo.activo = 1 AND
                    tipoSubareaPlantillaWhatsappGo.idTipoPlantilla = 1
        `, {
            type: QueryTypes.SELECT,
            plain: true,
        });
    }

    /*static async getNombreEmpleadoNombreProspecto (idSolicitud: number): Promise<any> {
        return await db.dbGoAgentes.query(`
            SELECT
                solicitudesAgentes.id AS idSolicitud,
                empleados.id AS idEmpleado,
                empleados.nombre,
                empleados.apellidoPaterno,
                empleados.apellidoMaterno,
                solicitudesAgentes.nombreCompleto AS nombreProspecto,
                solicitudesAgentes.telefono,
                solicitudesAgentes.idEmpleados
            FROM
                empleados
            JOIN
                    solicitudesAgentes ON empleados.id = solicitudesAgentes.idEmpleados
            WHERE
                    solicitudesAgentes.id = ${idSolicitud}
        `, {
            type: QueryTypes.SELECT,
            plain: true,
        });
    }*/

    static async getNombreEmpleadoNombreProspecto (idSolicitud: number): Promise<any> {
        // tslint:disable-next-line:only-arrow-functions
        return await new Promise(function (resolve, reject) {
            db.dbT2020.query(`
                SELECT
                    *
                FROM
                    empleado e
                JOIN` + ' ' + '`go-agentes`' + `.solicitudesAgentes s ON e.id = s.idEmpleados
                JOIN candidato ON e.idCandidato = candidato.id
                JOIN precandidato ON candidato.idPrecandidato = precandidato.id
                WHERE
                        s.id = ${idSolicitud}
            `, {
                type: QueryTypes.SELECT,
                plain: true,
            }).then(res => {
                resolve(res);
            }).catch(e => {
                reject(e);
            });
        });
    }

    static async getEmpleadoT2020(idEmpleado: number): Promise<any> {
        // tslint:disable-next-line:only-arrow-functions
        return await new Promise(function (resolve, reject) {
            db.dbT2020.query(`
                SELECT
                    empleado.id,
                    empleado.idSubarea,
                    subarea.idTipoSubarea
                FROM
                    empleado
                INNER JOIN subarea ON empleado.idSubarea = subarea.id
                WHERE
                    empleado.id = ${idEmpleado}
            `, {
                type: QueryTypes.SELECT,
                plain: true,
            }).then(em => {
                if (em === null) {
                    resolve({
                        mensaje: 'Empleado no encontrado',
                        error: em,
                        empleado: idEmpleado,
                    });
                }
                resolve(em);
            }).catch(e => {
                reject({
                    mensaje: e
                });
            });
        });
    }
}
