import db from "../../db/connection";
import {QueryTypes} from "sequelize";

export default class Empleados {
    static async getEmpleadoById(idEmpleado: number): Promise<any> { // falta hacer la vista de empleadoView en base de GO
        return await db.dbT2020.query(`
            SELECT
                // empleadoView
            FROM
                // empleadoView
            WHERE
                // empleadoView
        `, {
            type: QueryTypes.SELECT,
            plain: true,
        });
    }

    static async getEjecutivosGo (): Promise<any> {
        // tslint:disable-next-line:only-arrow-functions
        return await new Promise(function (resolve, reject) {
            db.dbT2020.query(`
                SELECT
                    e.id AS idEmpleado,
                    precandidato.nombre,
                    precandidato.apellidoPaterno,
                    precandidato.apellidoMaterno
                FROM
                    empleado e
                JOIN (SELECT *
                    FROM` + ' ' +  '`go-agentes`' + `.solicitudesAgentes sg
                    GROUP BY sg.idEmpleados
                    ORDER BY sg.id
                ) sgo ON sgo.idEmpleados = e.id
                JOIN candidato ON e.idCandidato = candidato.id
                JOIN precandidato ON candidato.idPrecandidato = precandidato.id
                ORDER BY e.id ASC
            `, {
                type: QueryTypes.SELECT
            }).then(res => {
                resolve(res);
            }).catch(e => {
                reject(e);
            });
        });
    }

    static async getEmpleadosMensajesWhatsappGo (idEmpleado: number, fechaInicio: any, fechaFin: any): Promise<any> {
        // tslint:disable-next-line:only-arrow-functions
        return await new Promise(function (resolve, reject) {
            db.dbT2020.query(`
                SELECT
                    e.id AS idEmpleado,
                    precandidato.nombre,
                    precandidato.apellidoPaterno,
                    precandidato.apellidoMaterno,
                    c.id AS idCanalWhatsapp,
                    c.idSolicitud as correlationId,
                    mw.fechaRegistro
                FROM
                    empleado e
                JOIN (SELECT *
                    FROM` + ' ' +  '`go-agentes`' + `.solicitudesAgentes sg
                    GROUP BY sg.idEmpleados
                    ORDER BY sg.id
                ) sgo ON sgo.idEmpleados = e.id
                JOIN` + ' ' + '`go-agentes`' + `.canalWhatsapp c ON sgo.id = c.idSolicitud
                JOIN (SELECT *
                    FROM` + ' ' +  '`go-agentes`' + `.mensajesWhatsapp mwh
                    GROUP BY mwh.idEmpleadoMensaje
                    ORDER BY mwh.id
                    DESC LIMIT 1
                ) mw ON mw.idCanalWhatsapp = c.id
                JOIN candidato ON e.idCandidato = candidato.id
                JOIN precandidato ON candidato.idPrecandidato = precandidato.id
                WHERE mw.idEmpleadoMensaje = ${idEmpleado} AND mw.fechaRegistro BETWEEN '${fechaInicio}' AND '${fechaFin}'
                ORDER BY e.id ASC
            `, {
                type: QueryTypes.SELECT
            }).then(res => {
                resolve(res);
            }).catch(e => {
                reject(e);
            });
        });
    }

    // static async getEmpleadosMensajesWhatsappGo (): Promise<any> {
    //     // tslint:disable-next-line:only-arrow-functions
    //     return await new Promise(function (resolve, reject) {
    //         db.dbT2020.query(`
    //             SELECT
    //                 c.id AS idCanalWhatsapp,
    //                 c.idSolicitud,
    //                 e.id AS idEmpleado,
    //                 mw.fechaRegistro,
    //                 precandidato.nombre,
    //                 precandidato.apellidoPaterno,
    //                 precandidato.apellidoMaterno
    //             FROM
    //                 empleado e
    //             JOIN` + ' ' + '`go-agentes`' + `.solicitudesAgentes s ON e.id = s.idEmpleados
    //             JOIN` + ' ' + '`go-agentes`' + `.canalWhatsapp c ON s.id = c.idSolicitud
    //             JOIN (SELECT *
    //                    FROM` + ' ' +  '`go-agentes`' + `.mensajesWhatsapp mwh
    //                    ORDER BY mwh.id
    //                    DESC LIMIT 1
    //             ) mw
    //             JOIN candidato ON e.idCandidato = candidato.id
    //             JOIN precandidato ON candidato.idPrecandidato = precandidato.id
    //         `, {
    //             type: QueryTypes.SELECT
    //         }).then(res => {
    //             resolve(res);
    //         }).catch(e => {
    //             reject(e);
    //         });
    //     });
    // }
}
