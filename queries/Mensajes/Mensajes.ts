import db from "../../db/connection";
import {QueryTypes} from "sequelize";
import MensajesWhatsapp from "../../models/MensajesWhatsapp";


export default class Mensajes {
    static async getMensajesByIdCanal(idCanal: number): Promise<any> {
        return await db.dbGoAgentes.query(`
            SELECT
                mensajesWhatsapp.*
            FROM
                mensajesWhatsapp
            WHERE
                mensajesWhatsapp.idCanalWhatsapp = ${idCanal}
        `, {type: QueryTypes.SELECT});
    }

    static async getSolicitudById(idSolicitud: number): Promise<any> {
        return await db.dbGoAgentes.query(`
            SELECT
                solicitudesAgentes.*
            FROM
                solicitudesAgentes
            WHERE
                solicitudesAgentes.id = ${idSolicitud}
        `, {
            type: QueryTypes.SELECT,
            plain: true,
        });
    }

    static async getMensajesByIdEmpleadoBandejaTipoEnvio(idEmpleado: number): Promise<any> {
        return await db.dbGoAgentes.query(`
            SELECT
                *
            FROM
                mensajesWhatsapp
            WHERE
                mensajesWhatsapp.idEmpleadoMensaje = ${idEmpleado} AND
                mensajesWhatsapp.idTipoEnvio = 2 AND
                mensajesWhatsapp.aBandeja = 1
        `, {type: QueryTypes.SELECT});
    }

    static async updateMensajesRespondidosVistos(idMensaje: number, data: MensajesWhatsapp): Promise<any> {
        return await MensajesWhatsapp.update({...data}, {
            where: {
                id: idMensaje,
            }
        });
    }

    static async getMensajesBandejaTipoEnvio(): Promise<any> {
        return await db.dbGoAgentes.query(`
            SELECT
                *
            FROM
                mensajesWhatsapp
            WHERE
                mensajesWhatsapp.idTipoEnvio = 2 AND
                mensajesWhatsapp.aBandeja = 1
        `, {type: QueryTypes.SELECT});
    }
}
