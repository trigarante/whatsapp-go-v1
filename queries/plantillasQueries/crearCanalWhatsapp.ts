import {Request, Response, Router} from "express";
import db from "../../db/connection";
import {QueryTypes} from "sequelize";
import MensajesWhatsapp from "../../models/MensajesWhatsapp";
import CanalWhatsapp from "../../models/CanalWhatsapp";
import {urlSockets} from "../../urlSockets";
// tslint:disable-next-line:no-var-requires
const request = require('request');

export default class CrearCanalWhatsapp {
    static async postCanalWhatsPlantillaExterno (req: Request, res: Response) {
        return await new Promise<any>(async (resolve, reject) => {
            let numeroDestino = req.body.messages.destinations[0].destination;
            const solicitud = req.body.messages.destinations[0].correlationId;
            const nombrePlantillaEnviada = req.body.messages.message.template.elementName;
            const idEmpleado = req.body.idEmpleado;
            const cuerpoPlantilla = req.body.cuerpoPlantilla;

            const { numeroGenerador, numerosRestantes } = req.body.credentials;

            // quita el 521 de los números para poder hacer la búsqueda en la tabla de solicitudes
            numeroDestino = numeroDestino.slice(3);

            let canalGuardado: any = {};

            try {
                const canalNuevo: any = new CanalWhatsapp();
                    canalNuevo.idEstadoCanal = 1;
                    canalNuevo.idSolicitud = solicitud;
                    canalNuevo.plantillaEnviada = nombrePlantillaEnviada,
                    canalNuevo.destino = numeroDestino;
                    // canalNuevo.fechaEnvio = new Date();
                    canalNuevo.origen = numeroGenerador;
                    canalNuevo.numerosRestantes = numerosRestantes;
                    canalNuevo.activacion = 1;

                canalGuardado = await CanalWhatsapp.create(canalNuevo.dataValues).then(dataCanalNuevo => dataCanalNuevo);

                const mensajesWhatsapp: any = new MensajesWhatsapp();
                    mensajesWhatsapp.idCanalWhatsapp = canalGuardado.id;
                    mensajesWhatsapp.idEmpleadoEnvio = idEmpleado;
                    mensajesWhatsapp.idTipoEnvio = 1; // SALIENTE
                    mensajesWhatsapp.idTipoMensajeWhatsapp = 7; // PLANTILLA
                    mensajesWhatsapp.mensaje = cuerpoPlantilla;
                    mensajesWhatsapp.correlationId = solicitud;
                    mensajesWhatsapp.aBandeja = 0;
                    mensajesWhatsapp.idEmpleadoMensaje = idEmpleado;

                    const mensajesWhats = await MensajesWhatsapp.create(mensajesWhatsapp.dataValues).then(dataMensaje => dataMensaje);

                    resolve({canalWhatsapp: canalGuardado, mensajesWhatsapp: mensajesWhats});
            } catch (e) {
                reject({error: e, mensaje: 'Data no creada'});
            }
        });
    }

    static async createCanalWhatsapp (req: Request, res: Response) {
        return await new Promise<any>(async (resolve, reject) => {
            const idEmpleado = +req.params.idEmpleados;
            const idSolicitudT2020 = +req.params.idSolicitud;

            // tslint:disable-next-line:only-arrow-functions
            const rParams = await Object.keys(req.params).some(function(idSolicitud) {
                return idSolicitud === 'idSolicitud';
            });
            await identificarTipoMensaje(req.body, rParams).then(async (msg: any) => {
                const mensajeTipos = msg;
                let jsonSolicitudes;
                let jsonHeader;

                const tipoMensajes: string = rParams ? req.body.dataSend[0].message.type : req.body.data[0].message.type;
                const cantidadMensajes = rParams ? req.body.dataSend.length : req.body.data.length;
                // tslint:disable-next-line:only-arrow-functions
                const tipoEnvio = Object.keys(req.body).some(function(tipo) {
                    return tipo === 'idTipoEnvio';
                });
                const idSolicitud = rParams ? req.body.dataSend[0].correlationId : req.body.data[0].correlationId;
                const data = await rParams ? req.body.dataSend[0] : req.body.data[0];
                const numero = await rParams ? req.body.dataSend[0].origin : data.source;
                tipoEnvio ? req.body = {...req.body, 'idEmpleadoEnvio': null} : req.body = {...req.body, 'idEmpleadoEnvio': req.body.idEmpleadoEnvio};
                // tslint:disable-next-line:only-arrow-functions
                const tipoMen = await Object.keys(req.body).some(function(tipoMsj) {
                    return tipoMsj === 'idTipoMensajeWhatsapp';
                });
                const aux = [];
                // solo la propiedad message. Se extraen los mensajes y se guardan en la variable aux
                for (let i = 0; i < cantidadMensajes; i++) {
                    aux.push(mensajeTipos);
                }

                try {
                    const { idTipoMensajeWhatsapp } = await this.getTipoMensajeWhatsapp(tipoMensajes).catch(e => {
                        return console.log({mensaje: `Tipo mensaje ${tipoMensajes} no encontrado`, error: e});
                    });

                    const mensajesWhatsappData: any = await this.getMensajesWhatsapp(+idSolicitud).catch(e => {
                            return console.log({mensaje: `mensajes con solicitud ${idSolicitud} no encontrados`, error: e});
                        });

                    const canalWhatsappData: CanalWhatsapp = await this.getCanalWhatsapp(idSolicitud).catch(e => {
                        return console.log({mensaje: `canal con solicitud ${idSolicitud} no encontrado`, error: e});
                    });

                    if (canalWhatsappData) {
                        const mensajesWhatsapp: any = new MensajesWhatsapp();
                        mensajesWhatsapp.idCanalWhatsapp = canalWhatsappData.id;
                        mensajesWhatsapp.idEmpleadoEnvio = req.body.idEmpleadoEnvio;
                        mensajesWhatsapp.idTipoEnvio = tipoEnvio ? 2 : 1;
                        mensajesWhatsapp.idTipoMensajeWhatsapp = tipoMen ? 2 : idTipoMensajeWhatsapp;
                        mensajesWhatsapp.mensaje = mensajeTipos;
                        mensajesWhatsapp.correlationId = rParams ? req.body.dataSend[0].correlationId : req.body.data[0].correlationId;
                        mensajesWhatsapp.aBandeja = tipoEnvio ? 1 : 0;
                        mensajesWhatsapp.idEmpleadoMensaje = idEmpleado;

                        if (mensajesWhatsappData && mensajesWhatsappData.length > 0) {
                            mensajesWhatsapp.idCanalWhatsapp = mensajesWhatsappData[0].idCanalWhatsapp;
                        }

                        const mensajeRegistrado: any = await this.saveMensaje({...mensajesWhatsapp.dataValues})
                            .catch(e => {
                            return console.log({mensaje: 'Data de mensaje no registrada en la tabla MensajesWhatsapp', error: e});
                        });

                        const jsonCantidadMensajesIndividuales = await counter(idEmpleado);
                        if (mensajeRegistrado.idTipoEnvio === 2) {

                            try {
                                const mensajes = await this.getMensajesWhatsappEnBandejaAndTipoEnvio(idEmpleado);
                                // const mensajesFiltroHora = mensajes.filter((dataMensajes: any) => {
                                //     const date = new Date(dataMensajes.fecha);
                                //     const dateConHoras = new Date(dataMensajes.fecha).setHours(24);
                                //     const dateActual = new Date();
                                //     const dateMsecActual = dateActual.getTime();
                                //
                                //     const fechasData = timeConversion(date, dateConHoras, dateMsecActual);
                                //     return fechasData.milisecActual < fechasData.milisecFechaRegistroMasHoras;
                                // });
                                const noRespondidosArr: any[] = [];
                                mensajes.forEach((dataMsg: any) => {
                                    noRespondidosArr.push(dataMsg.correlationId);
                                });
                                const setNoRespondidosArr = new Set(noRespondidosArr);
                                const dataNoRepetida = noRespondidosArr.filter(onlyUnique);
                                if (mensajes) {

                                    sendSocket({
                                        mensajes: dataNoRepetida,
                                        cantidadNotificaciones: setNoRespondidosArr.size,
                                        cantidadMensajesIndividuales: jsonCantidadMensajesIndividuales,
                                        idEmpleado,
                                        mensajeRegistrado: {...mensajeRegistrado, fecha: new Date()},
                                        canalWhatsappData,
                                        socket: 'headerGo',
                                    }, res);

                                }
                                // console.log(!!idSolicitudT2020, 'idSolicitudT2020idSolicitudT2020');
                                if (idSolicitudT2020) {
                                    res.send({canalWhatsapp: canalWhatsappData, mensajesWhatsapp: mensajeRegistrado});
                                    return;
                                }
                            } catch (e) {
                                console.log(e);
                            }
                        }
                        if (idSolicitudT2020) {
                            res.send({canalWhatsapp: canalWhatsappData, mensajesWhatsapp: mensajeRegistrado});
                            return;
                        }
                    } else {
                        if (idSolicitudT2020) {
                            res.status(404).send({error: `Canal no encontrado con la solicitud ${idSolicitud}`});
                            return;
                        }
                    }
                } catch (e) {
                    // res.send(e);
                    reject(e);
                }
            });
        });

        // Funciones

        function identificarTipoMensaje(reeq: any, flag: any) {
            // tslint:disable-next-line:only-arrow-functions
            return new Promise( function (resolve, reject) {
                if (flag) {
                    switch (reeq.dataSend[0].message.type) {
                        case "TEXT":
                            resolve(reeq.dataSend[0].message.messageText);
                            break;

                        case "IMAGE":
                        case "AUDIO":
                        case "DOCUMENT":
                        case "STICKER":
                            resolve(reeq.dataSend[0].message.mediaUrl);
                            break;

                        case "LOCATION":
                            resolve(reeq.dataSend[0].message.location.geoPoint);
                            break;

                        default:
                            reject('ERROR');
                    }
                } else {
                    switch (reeq.data[0].message.type) {
                        case "TEXT":
                            resolve(reeq.data[0].message.messageText);
                            break;

                        case "IMAGE":
                        case "AUDIO":
                        case "DOCUMENT":
                        case "STICKER":
                            resolve(reeq.data[0].message.mediaUrl);
                            break;

                        case "LOCATION":
                            resolve(reeq.data[0].message.location.geoPoint);
                            break;

                        default:
                            reject('ERROR');
                    }
                }
            })
        }

        async function counter(idEmpleado?: number) {
            const idEmpleadoEnvio: number | undefined = idEmpleado;
            const mensajes: any = await CrearCanalWhatsapp.getMensajesWhatsappEnBandejaAndTipoEnvioAndEmpleado(idEmpleadoEnvio);
            // const mensajesFiltroHora = mensajes.filter((data: any) => {
            //     const date = new Date(data.fecha);
            //     const dateConHoras = new Date(data.fechaRegistro).setHours(24);
            //     const dateActual = new Date();
            //     const dateMsecActual = dateActual.getTime();
            //
            //     const fechasData = conversion(date, dateConHoras, dateMsecActual);
            //     return fechasData.milisecActual < fechasData.milisecFechaRegistroMasHoras;
            // });
            const noRespondidosArr: any = [];
            mensajes.forEach((dataMsg: any) => {
                noRespondidosArr.push(dataMsg.correlationId);
            });
            /* cuenta cuantos mensajes tienen por responder cada canal
             { '161552': 1, '161553': 1, '161555': 2 } */
            const noRespondidosArrSorted: any = noRespondidosArr.sort();
            const counterJson: any = {};
            // tslint:disable-next-line:prefer-for-of
            for (let i = 0; i < noRespondidosArrSorted.length; i++) {
                if (!(noRespondidosArrSorted[i] in counterJson)) counterJson[noRespondidosArrSorted[i]] = 0;
                counterJson[noRespondidosArrSorted[i]]++;
            }


            return counterJson;
        }

        function conversion(fecha: any, fechaMasHora: any, dateMsec: any) {
            const msecPerMinute = 1000 * 60;
            const msecPerHour = msecPerMinute * 60;
            const msecPerDay = msecPerHour * 24;

            const milisecFechaRegistro = fecha / msecPerDay;
            const milisecFechaRegistroMasHoras = fechaMasHora / msecPerDay;
            const milisecActual = dateMsec / msecPerDay;

            return {
                milisecFechaRegistro,
                milisecFechaRegistroMasHoras,
                milisecActual
            }
        }

        function onlyUnique(value: any, index: any, self: any) {
            return self.indexOf(value) === index;
        }

        function timeConversion(fecha: any, fechaMasHora: any, dateMsec: any) {
            const msecPerMinute = 1000 * 60;
            const msecPerHour = msecPerMinute * 60;
            const msecPerDay = msecPerHour * 24;

            const milisecFechaRegistro = fecha / msecPerDay;
            const milisecFechaRegistroMasHoras = fechaMasHora / msecPerDay;
            const milisecActual = dateMsec / msecPerDay;

            return {
                milisecFechaRegistro,
                milisecFechaRegistroMasHoras,
                milisecActual
            }
        }

        function sendSocket(reqSocket: any, resSocket: any) {
            const tipoSocket = reqSocket.socket;
            let tipoPath;
            const path = urlSockets.PATH;
            switch (tipoSocket) {
                case 'headerGo':
                    tipoPath = '/header-go';
                    break;
            }

            request({
                    url: path + tipoPath,
                    method: 'POST',
                    json: true,
                    body: reqSocket,
                },
                // tslint:disable-next-line:only-arrow-functions
                function (error: any, response: any, body: any) {
                    if (response) {
                        console.log(response.body, 'response...');
                    }
                    if (body) {
                        // console.log(body, 'body******************************');
                    }
                });
        }
    }

    static async getTipoMensajeWhatsapp(tipoMensaje: string): Promise<any> {
        return await db.dbGoAgentes.query(`
            SELECT
                tipoMensajeWhatsapp.id AS idTipoMensajeWhatsapp,
                tipoMensajeWhatsapp.tipoMensaje
                FROM
                tipoMensajeWhatsapp
                WHERE
                tipoMensajeWhatsapp.tipoMensaje = '${tipoMensaje}'
        `, {
            type: QueryTypes.SELECT,
            plain: true,
        });
    }

    static async getMensajesWhatsapp(idSolicitud: number): Promise<any> {
        // tslint:disable-next-line:only-arrow-functions
        return await new Promise(function (resolve, reject) {
                    db.dbGoAgentes.query(`
            SELECT
                mensajesWhatsapp.id,
                mensajesWhatsapp.idCanalWhatsapp,
                mensajesWhatsapp.idTipoMensajeWhatsapp,
                mensajesWhatsapp.mensaje,
                mensajesWhatsapp.idEmpleadoEnvio,
                mensajesWhatsapp.idTipoEnvio,
                mensajesWhatsapp.correlationId,
                mensajesWhatsapp.fechaRegistro,
                mensajesWhatsapp.aBandeja,
                mensajesWhatsapp.idEmpleadoMensaje
            FROM
                mensajesWhatsapp
            WHERE
                mensajesWhatsapp.correlationId = ${idSolicitud}
        `, {
                        type: QueryTypes.SELECT,
                        // plain: true,
                    }).then(r => {
                        resolve(r);
                    }).catch(e => {
                        reject({data: [], error: e});
                    })
        });
    }

    static async getCanalWhatsapp(idSolicitud: number): Promise<any> {
        return await db.dbGoAgentes.query(`
            SELECT
                *
            FROM
                canalWhatsapp
            WHERE
                canalWhatsapp.idSolicitud = ${idSolicitud}
        `, {
            type: QueryTypes.SELECT,
            plain: true,
        });
    }

    static async getMensajesWhatsappEnBandejaAndTipoEnvioAndEmpleado(idEmpleadoMensaje: number | undefined): Promise<any> {
        // tslint:disable-next-line:only-arrow-functions
        return await new Promise(function (resolve, reject) {
            db.dbGoAgentes.query(`
                SELECT
                    mensajesWhatsapp.id,
                    mensajesWhatsapp.idCanalWhatsapp,
                    mensajesWhatsapp.idTipoMensajeWhatsapp,
                    mensajesWhatsapp.mensaje,
                    mensajesWhatsapp.idEmpleadoEnvio,
                    mensajesWhatsapp.idTipoEnvio,
                    mensajesWhatsapp.correlationId,
                    mensajesWhatsapp.fechaRegistro,
                    mensajesWhatsapp.aBandeja,
                    mensajesWhatsapp.idEmpleadoMensaje
                FROM
                    mensajesWhatsapp
                WHERE
                    mensajesWhatsapp.aBandeja = 1 AND
                    mensajesWhatsapp.idTipoEnvio = 2 AND
                    mensajesWhatsapp.idEmpleadoMensaje = ${idEmpleadoMensaje}
            `, {
                type: QueryTypes.SELECT,
            }).then(r => {
                resolve(r);
            }).catch(e => {
                reject({data: [], error: e});
            });
        });
    }

    static async getMensajesWhatsappEnBandejaAndTipoEnvio(idEmpleado: any): Promise<any> {
        return await db.dbGoAgentes.query(`
            SELECT
                mensajesWhatsapp.id,
                mensajesWhatsapp.idCanalWhatsapp,
                mensajesWhatsapp.idTipoMensajeWhatsapp,
                mensajesWhatsapp.mensaje,
                mensajesWhatsapp.idEmpleadoEnvio,
                mensajesWhatsapp.idTipoEnvio,
                mensajesWhatsapp.correlationId,
                mensajesWhatsapp.fechaRegistro,
                mensajesWhatsapp.aBandeja,
                mensajesWhatsapp.idEmpleadoMensaje
            FROM
                mensajesWhatsapp
            WHERE
                mensajesWhatsapp.aBandeja = 1 AND
                mensajesWhatsapp.idTipoEnvio = 2 AND
                mensajesWhatsapp.idEmpleadoMensaje = ${idEmpleado}
        `, {type: QueryTypes.SELECT});
    }

    static async saveMensaje(mensajeData: MensajesWhatsapp) {
        return await MensajesWhatsapp.create({...mensajeData}).then((d: any) => d.dataValues);
    }

    static async saveCanal(canalData: CanalWhatsapp) {
        console.log('ESTOY EN EL saveCanal');
        return await CanalWhatsapp.create({...canalData}).then((d: any) => d.dataValues);
    }

    static async getPlantillaByIdEmpleadoTipoSubarea(idTipoSubarea: number): Promise<any> {
        return await db.dbGoAgentes.query(`
            SELECT
                plantillaComercialWhatsapp.id,
                plantillaComercialWhatsapp.nombrePlantilla,
                plantillaComercialWhatsapp.cuerpo,
                plantillaComercialWhatsapp.activa,
                plantillaComercialWhatsapp.idTipoSubarea
            FROM
                plantillaComercialWhatsapp
            WHERE
                plantillaComercialWhatsapp.idTipoSubarea = ${ idTipoSubarea } AND
                plantillaComercialWhatsapp.activa = 1
        `, {
            type: QueryTypes.SELECT,
            plain: true,
        });
    }
}
