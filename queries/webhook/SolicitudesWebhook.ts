import db from "../../db/connection";
import {Op, QueryTypes} from "sequelize";

export default class SolicitudesWebhook {
    static async getSolicitudesCompletas (idSolicitud: number): Promise<any> {
        return await db.dbGoAgentes.query(`
            SELECT
                solicitudesAgentes.id,
                solicitudesAgentes.nombreCompleto,
                solicitudesAgentes.correo,
                solicitudesAgentes.telefono,
                solicitudesAgentes.idEstadoSolicitud,
                solicitudesAgentes.cedula,
                solicitudesAgentes.cp,
                solicitudesAgentes.fechaCreacion,
                solicitudesAgentes.idPaginas,
                solicitudesAgentes.idEmpleados,
                solicitudesAgentes.edad
                FROM
                solicitudesAgentes
                WHERE
                solicitudesAgentes.id = ${idSolicitud}
        `, {
            type: QueryTypes.SELECT,
            plain: true,
        });
    }
}
