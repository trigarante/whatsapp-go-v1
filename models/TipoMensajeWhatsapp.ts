import {Sequelize, Model, DataTypes} from "sequelize";
import db from "../db/connection";

const sequelize = db.dbGoAgentes;

class TipoMensajeWhatsapp extends Model {
    id!: number;
    tipoMensaje!: string;
}

TipoMensajeWhatsapp.init({
    id: {
        type: DataTypes.INTEGER.UNSIGNED,
        autoIncrement: true,
        primaryKey: true,
    },
    tipoMensaje: {
        type: DataTypes.INTEGER
    },
}, {
    tableName: "tipoMensajeWhatsapp",
    timestamps: false,
    sequelize,
});

export default TipoMensajeWhatsapp;
