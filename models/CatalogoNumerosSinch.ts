import {Sequelize, Model, DataTypes} from "sequelize";
import db from "../db/connection";

const sequelize = db.dbGoAgentes;

class CatalogoNumerosSinch extends Model {
    id!: number;
    numeroTelefono!: number;
    tipo!: string;
    marca!: string;
    namespace!: string;
    userName!: string;
    authToken!: string;
    idMarca!: number;
}

CatalogoNumerosSinch.init({
    id: {
        type: DataTypes.INTEGER.UNSIGNED,
        autoIncrement: true,
        primaryKey: true,
    },
    numeroTelefono: {
        type: DataTypes.INTEGER
    },
    tipo: {
        type: DataTypes.STRING
    },
    marca: {
        type: DataTypes.STRING
    },
    namespace: {
        type: DataTypes.STRING
    },
    userName: {
        type: DataTypes.STRING
    },
    authToken: {
        type: DataTypes.STRING
    },
    idMarca: {
        type: DataTypes.INTEGER
    },
}, {
    tableName: "catalogoNumerosSinch",
    timestamps: false,
    sequelize,
});
