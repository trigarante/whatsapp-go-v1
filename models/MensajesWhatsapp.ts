import {Sequelize, Model, DataTypes} from "sequelize";
import db from "../db/connection";

const sequelize = db.dbGoAgentes;

class MensajesWhatsapp extends Model{
    id!: number;
    idCanalWhatsapp!: number;
    idTipoMensajeWhatsapp!: number;
    mensaje!: string;
    idEmpleadoEnvio!: number;
    idTipoEnvio!: number;
    correlationId!: number;
    aBandeja!: number;
    idEmpleadoMensaje!: number;
}

MensajesWhatsapp.init({
    id: {
        type: DataTypes.INTEGER.UNSIGNED,
        autoIncrement: true,
        primaryKey: true,
    },
    idCanalWhatsapp: {
        type: DataTypes.INTEGER
    },
    idTipoMensajeWhatsapp: {
        type: DataTypes.INTEGER
    },
    mensaje: {
        type: DataTypes.STRING
    },
    idEmpleadoEnvio: {
        type: DataTypes.INTEGER
    },
    idTipoEnvio: {
        type: DataTypes.INTEGER
    },
    correlationId: {
        type: DataTypes.INTEGER
    },
    aBandeja: {
        type: DataTypes.INTEGER
    },
    idEmpleadoMensaje: {
        type: DataTypes.INTEGER
    },
}, {
    tableName: "mensajesWhatsapp",
    timestamps: false,
    sequelize,
});

export default MensajesWhatsapp;
