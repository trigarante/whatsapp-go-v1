import {Sequelize, Model, DataTypes} from "sequelize";
import db from "../db/connection";

const sequelize = db.dbGoAgentes;

class CallbackSinch extends Model {
    id!: number;
    data!: any;
    idSinch!: string;
    idSolicitud!: number;
}

CallbackSinch.init({
    id: {
        type: DataTypes.INTEGER.UNSIGNED,
        autoIncrement: true,
        primaryKey: true,
    },
    data: {
        type: DataTypes.JSON
    },
    total: {
        type: DataTypes.INTEGER
    },
    idSinch: {
        type: DataTypes.STRING
    },
    idSolicitud: {
        type: DataTypes.INTEGER
    }
},
    {
        tableName: "callbackSinch",
        timestamps: false,
        sequelize,
    });

export default CallbackSinch;
