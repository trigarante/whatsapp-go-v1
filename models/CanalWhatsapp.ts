import {Sequelize, Model, DataTypes} from "sequelize";
import db from "../db/connection";

const sequelize = db.dbGoAgentes;

class CanalWhatsapp extends Model{
    id!: number;
    idSolicitud!: number;
    idEstadoCanal!: number;
    destino!: number;
    origen!: number;
    fechaEnvio!: any;
    plantillaEnviada!: string;
    numerosRestantes!: any;
    activacion!: number;
}

CanalWhatsapp.init({
    id: {
        type: DataTypes.INTEGER.UNSIGNED,
        autoIncrement: true,
        primaryKey: true,
    },
    idSolicitud: {
        type: DataTypes.INTEGER
    },
    idEstadoCanal: {
        type: DataTypes.INTEGER
    },
    destino: {
        type: DataTypes.INTEGER
    },
    origen: {
        type: DataTypes.INTEGER
    },
    fechaEnvio: {
        type: DataTypes.DATE
    },
    plantillaEnviada: {
        type: DataTypes.STRING
    },
    numerosRestantes: {
        type: DataTypes.JSON
    },
    activacion: {
        type: DataTypes.INTEGER
    }
}, {
    tableName: "canalWhatsapp",
    timestamps: false,
    sequelize,
});

export default CanalWhatsapp;
